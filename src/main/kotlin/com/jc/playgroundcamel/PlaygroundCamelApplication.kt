package com.jc.playgroundcamel

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PlaygroundCamelApplication

fun main(args: Array<String>) {
	runApplication<PlaygroundCamelApplication>(*args)
}
