package com.jc.playgroundcamel

import org.apache.camel.ExchangePattern
import org.apache.camel.ProducerTemplate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class MyController {
    @Autowired
    private lateinit var producerTemplate: ProducerTemplate

    private val logger = LoggerFactory.getLogger(this.javaClass)

    // curl -X POST http://localhost:7700/api/claimSubmission -H 'Content-Type: application/json' -d '{"parentId":"S1","childId":"T1","numberOfDays":10,"salaryPerDay":5000}'
    @PostMapping("/claimSubmission")
    fun requestAgencyData(@RequestBody claimSubmission: SubmissionDTO): String {
        logger.info("submission received: $claimSubmission")
        val response = producerTemplate.sendBody("direct:claim-submission", ExchangePattern.InOut, claimSubmission) as ByteArray
        return String(response)
    }

    @GetMapping("/fsr/retrieveChild/{parentId}")
    fun requestFsrData(@PathVariable parentId: String): FsrResponseDTO {
        logger.info("request fsr child for parent: $parentId")
        val childId = if (parentId == "S1") "T1" else "T99"
        val fsrResponseDTO = FsrResponseDTO(
            parentId = parentId,
            spouseId = "ABC",
            childId = childId
        )
        return fsrResponseDTO
    }
}