package com.jc.playgroundcamel

import org.apache.camel.AggregationStrategy
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.dataformat.JsonLibrary
import org.apache.camel.model.rest.RestBindingMode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

//@Component
//class MyRouter: RouteBuilder() {
//    @Autowired
//    private lateinit var getCurrentTimeBean: GetCurrentTimeBean
//
//    override fun configure() {
//        restConfiguration().host("localhost").port(7500).bindingMode(RestBindingMode.json)
//
//        from("timer:first-timer")
//            .transform().constant("""{"parentId":"S1199","childId":"T1"}""")
//            .setHeader("parentId", jsonpath("$.parentId"))
//            .to("log:first-timerddd")
//            .to("rest:get:/fsr/retrieveChild/{parentId}")
//            .log("tmd: \${body}")
//    }
//}

//region Retrieve Child Routes
@Component
class MyIcaRequestRoute: RouteBuilder() {
    @Autowired
    private lateinit var myIcaResponseBean: MyIcaResponseBean

    override fun configure() {
        from("direct:ica-child-request")
            .log("ica-child-request before marshal: \${body}")
            .setHeader("parentId", jsonpath("$..parentId"))
            // create the input file request
            .marshal().json(JsonLibrary.Jackson, OdmMaxClaimDTO::class.java)
            .log("ica-child-request after marshal: \${body}")
            .to("file:files/input/?filename=request.txt")
            // simulate the output file response immediately
            .unmarshal().json(JsonLibrary.Jackson, OdmMaxClaimDTO::class.java)
            .log("ica-child-request after unmarshal: \${body}")
            .bean(myIcaResponseBean, "getChildren")
            .log("ica-child-request after bean: \${body}")
            .marshal().json(JsonLibrary.Jackson, IcaResponseDTO::class.java)
            .to("file:files/output/?filename=response.txt")
    }
}

@Component
class MyFsrRequestRoute: RouteBuilder() {
    override fun configure() {
        from("direct:fsr-child-request")
            .log("fsr-child-request: \${body}")
            .setHeader("parentId", jsonpath("$..parentId"))
            .to("rest:get:/api/fsr/retrieveChild/{parentId}/?host=localhost:7700")
            .log("in fsr-child-request after rest: \${body}")
    }
}

@Component
class MyRetrieveChildRoute: RouteBuilder() {
    override fun configure() {
        from("direct:retrieve-child")
            .log("retrieve-child: \${body}")
            .multicast(MyAggregateStrategy()).parallelProcessing()
            .to("direct:ica-child-request")
            .to("direct:fsr-child-request")
            .end()
            .log("consolidate-child after completion: \${body}")
    }
}
//endregion

//region Claim Submission Routes
@Component
class MySubmissionRoute: RouteBuilder() {
    override fun configure() {
        restConfiguration().host("localhost").port(9060).bindingMode(RestBindingMode.json)
        from("direct:claim-submission") // SubmissionDTO
            .log("claim-submission before process: \${body}")
            .process(MySubmissionProcessor()) // OdmSubmissionDTO
            .log("claim-submission after process: \${body}")
            .to("rest:post:/DecisionService/rest/deployClaimRule/flowClaimCalculation") // api call
            .log("claim-submission after odm api call: \${body}") // json string
            .unmarshal().json(JsonLibrary.Jackson, OdmMaxClaimDTO::class.java) // OdmMaxClaimDTO
            .log("claim-submission after odm max claim: \${body}")
            .setHeader("odmClaimCalculationResponse", simple("\${body}"))
            .log("saved header: \${header.odmClaimCalculationResponse}")
            .to("direct:retrieve-child") // retrieve child from ICA and FSR in parallel
            .log("after aggregation: \${body}")
            .unmarshal().json(JsonLibrary.Jackson, AggregatedChildResponseDTO::class.java) // AggregatedChildResponseDTO
            .log("after marshal: \${body}")
            .process(MyValidationProcessor()) // OdmValidationDTO
            .log("after process: \${body}")
            .to("rest:post:/DecisionService/rest/deployClaimRule/flowValidation") // api call
            .log("claim status: \${body}")
    }
}
//endregion

class MyAggregateStrategy: AggregationStrategy {
    override fun aggregate(oldExchange: Exchange?, newExchange: Exchange?): Exchange {
        if (oldExchange == null) {
            return newExchange!!
        }
        val oldBody = oldExchange.getIn().getBody(String::class.java)
        val newBody = newExchange!!.getIn().getBody(String::class.java)
        println("oldBody is: $oldBody")
        println("newBody is: $newBody")
        val combineResponse = "{\"ica\":$oldBody,\"fsr\":$newBody}"
        oldExchange.getIn().body = combineResponse
        return oldExchange
    }
}

class MySubmissionProcessor : Processor {
    override fun process(exchange: Exchange?) {
        val submissionDTO = exchange!!.message.body as SubmissionDTO
        val odmSubmissionDetailsDTO = OdmSubmissionDetailsDTO(
            submissionDTO.parentId,
            submissionDTO.childId,
            submissionDTO.numberOfDays,
            submissionDTO.salaryPerDay
        )
        val odmSubmissionDTO = OdmSubmissionDTO(odmSubmissionDetailsDTO)
        exchange.message.body = odmSubmissionDTO
    }
}

class MyValidationProcessor : Processor {
    override fun process(exchange: Exchange?) {
        val aggregatedChildResponseDTO = exchange!!.message.body as AggregatedChildResponseDTO
        val odmMaxClaimDTO = exchange.message.getHeader("odmClaimCalculationResponse") as OdmMaxClaimDTO
        val odmValidationDetailsDTO = OdmValidationDetailsDTO(
            maxClaimAmount = odmMaxClaimDTO.myMaxClaimDetails.maxClaimAmount,
            parentId = odmMaxClaimDTO.myMaxClaimDetails.parentId,
            childId = odmMaxClaimDTO.myMaxClaimDetails.childId,
            icaChildId = aggregatedChildResponseDTO.ica.childId,
            fsrChildId = aggregatedChildResponseDTO.fsr.childId,
            fsrSpouseId = aggregatedChildResponseDTO.fsr.spouseId
        )
        val odmValidationDTO = OdmValidationDTO(odmValidationDetailsDTO)
        exchange.message.body = odmValidationDTO
    }
}