package com.jc.playgroundcamel

import java.time.LocalDateTime
import org.springframework.stereotype.Component

@Component
class GetCurrentTimeBean {
    fun getCurrentTime() = "Time now is " + LocalDateTime.now()
}

@Component
class MyIcaResponseBean {
    fun getChildren(odmMaxClaimDTO: OdmMaxClaimDTO): IcaResponseDTO {
        val icaResponseDTO = IcaResponseDTO()
        if (odmMaxClaimDTO.myMaxClaimDetails.parentId == "S1") {
            icaResponseDTO.parentId = odmMaxClaimDTO.myMaxClaimDetails.parentId
            icaResponseDTO.childId = "T1"
        }
        else {
            icaResponseDTO.parentId = odmMaxClaimDTO.myMaxClaimDetails.parentId
            icaResponseDTO.childId = "T88"
        }
        return icaResponseDTO
    }
}